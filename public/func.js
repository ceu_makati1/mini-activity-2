document.getElementById("add").addEventListener('click', Addition);
document.getElementById("sub").addEventListener('click', Subtraction);
document.getElementById("mul").addEventListener('click', Multiplication);
document.getElementById("div").addEventListener('click', Division);

function Addition(){
    let num1 = Number(document.getElementById("num1").value);
    let num2 = Number(document.getElementById("num2").value);

    let answer = num1 + num2;

    document.getElementById("result").innerHTML = answer;
}

function Subtraction(){
    let num1 = Number(document.getElementById("num1").value);
    let num2 = Number(document.getElementById("num2").value);

    let answer = num1 - num2;

    document.getElementById("result").innerHTML = answer;
}

function Multiplication(){
    let num1 = Number(document.getElementById("num1").value);
    let num2 = Number(document.getElementById("num2").value);

    let answer = num1 * num2;

    document.getElementById("result").innerHTML = answer;
}

function Division(){
    let num1 = Number(document.getElementById("num1").value);
    let num2 = Number(document.getElementById("num2").value);

    let answer = num1 / num2;

    document.getElementById("result").innerHTML = answer;
}